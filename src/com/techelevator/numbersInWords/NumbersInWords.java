package com.techelevator.numbersInWords;

public class NumbersInWords {

	public static String numberToWords(int number) {
		String[] arrayOfNumberWords = new String[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
				"Eigth", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
				"Eighteen", "Nineteen", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety",
				"Hundred" };

		if (number >= 100) {
			String numberInWords = "";
			int hundredsDigit = getHundredsDigit(number);
			int tensDigit = getTensDigit(number);
			numberInWords += arrayOfNumberWords[hundredsDigit] + " Hundred";
			if (number > 100 && number <= 120) {
				numberInWords += " " + arrayOfNumberWords[number - 100];
			} else if (number > 120) {
				if (getTensDigit(number) != 0) {
					numberInWords += " " + arrayOfNumberWords[getTensDigitWord(tensDigit)];
				}
				if (getOnesDigit(number) != 0) {
					numberInWords += " " + arrayOfNumberWords[getOnesDigit(number)];
				}
			}

			return numberInWords;
		}
		if (number > 20) {
			int tensDigit = getTensDigit(number);
			if (getOnesDigit(number) != 0) {
				return arrayOfNumberWords[getTensDigitWord(tensDigit)] + " " + arrayOfNumberWords[getOnesDigit(number)];
			} else if (getOnesDigit(number) == 0) {
				return arrayOfNumberWords[getTensDigitWord(tensDigit)];
			}
		}

		return arrayOfNumberWords[number];

	}

	private static int getHundredsDigit(int number) {
		return number / 100;
	}

	private static int getOnesDigit(int number) {
		return number % 10;
	}

	private static int getTensDigitWord(int tensDigit) {
		return tensDigit + 18;
	}

	private static int getTensDigit(int number) {
		return (number / 10) % 10;
	}
}
