package com.techelevator.numbersInWords;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumbersInWordsTest {

	@Test
	public void string_returns_number_under_twenty_in_words() {
		assertEquals("Zero", NumbersInWords.numberToWords(0));
		assertEquals("One", NumbersInWords.numberToWords(1));
		assertEquals("Fifteen", NumbersInWords.numberToWords(15));	
		assertEquals("Nineteen", NumbersInWords.numberToWords(19));
	}
	
	@Test 
	public void string_returns_number_in_twenties_in_words() {
		assertEquals("Twenty", NumbersInWords.numberToWords(20));
		assertEquals("Twenty One", NumbersInWords.numberToWords(21));
		assertEquals("Twenty Five", NumbersInWords.numberToWords(25));
		assertEquals("Twenty Nine", NumbersInWords.numberToWords(29));
	}
	
	@Test 
	public void string_returns_number_in_thirties_in_words() {
		assertEquals("Thirty", NumbersInWords.numberToWords(30));
		assertEquals("Thirty One", NumbersInWords.numberToWords(31));
		assertEquals("Thirty Five", NumbersInWords.numberToWords(35));
		assertEquals("Thirty Nine", NumbersInWords.numberToWords(39));
	}
	
	@Test
	public void string_returns_number_in_forties_in_words() {
		assertEquals("Forty", NumbersInWords.numberToWords(40));
		assertEquals("Forty One", NumbersInWords.numberToWords(41));
		assertEquals("Forty Five", NumbersInWords.numberToWords(45));
		assertEquals("Forty Nine", NumbersInWords.numberToWords(49));
	}
	
	@Test 
	public void string_returns_number_in_seventies_in_words() {
		assertEquals("Seventy", NumbersInWords.numberToWords(70));
		assertEquals("Seventy One", NumbersInWords.numberToWords(71));
		assertEquals("Seventy Five", NumbersInWords.numberToWords(75));
		assertEquals("Seventy Nine", NumbersInWords.numberToWords(79));
	}
	
	@Test 
	public void string_returns_number_in_nineties_in_words() {
		assertEquals("Ninety", NumbersInWords.numberToWords(90));
		assertEquals("Ninety One", NumbersInWords.numberToWords(91));
		assertEquals("Ninety Five", NumbersInWords.numberToWords(95));
		assertEquals("Ninety Nine", NumbersInWords.numberToWords(99));
	}
	
	@Test
	public void string_returns_number_over_one_hundred_in_words() {
		assertEquals("One Hundred", NumbersInWords.numberToWords(100));
		assertEquals("One Hundred One", NumbersInWords.numberToWords(101));
		assertEquals("One Hundred Five", NumbersInWords.numberToWords(105));
		assertEquals("One Hundred Ten", NumbersInWords.numberToWords(110));
		assertEquals("One Hundred Fifteen", NumbersInWords.numberToWords(115));
	}
	
	@Test
	public void string_returns_number_in_one_hundres_twenties_in_words() {
		assertEquals("One Hundred Twenty", NumbersInWords.numberToWords(120));
		assertEquals("One Hundred Twenty One", NumbersInWords.numberToWords(121));
		assertEquals("One Hundred Twenty Five", NumbersInWords.numberToWords(125));
		assertEquals("One Hundred Twenty Nine", NumbersInWords.numberToWords(129));	
	}
	
	@Test
	public void string_returns_number_in_one_hundres_sixties_in_words() {
		assertEquals("One Hundred Sixty", NumbersInWords.numberToWords(160));
		assertEquals("One Hundred Sixty One", NumbersInWords.numberToWords(161));
		assertEquals("One Hundred Sixty Five", NumbersInWords.numberToWords(165));
		assertEquals("One Hundred Sixty Nine", NumbersInWords.numberToWords(169));	
	}
	
	@Test
	public void string_returns_number_in_two_hundreds_in_words() {
		assertEquals("Two Hundred", NumbersInWords.numberToWords(200));
		assertEquals("Two Hundred Five", NumbersInWords.numberToWords(205));
		assertEquals("Two Hundred Thirty Four", NumbersInWords.numberToWords(234));
		assertEquals("Two Hundred Seventy", NumbersInWords.numberToWords(270));
	}
	
	@Test
	public void string_returns_number_in_nine_hundreds_in_words() {
		assertEquals("Nine Hundred", NumbersInWords.numberToWords(900));
		assertEquals("Nine Hundred Five", NumbersInWords.numberToWords(905));
		assertEquals("Nine Hundred Thirty Four", NumbersInWords.numberToWords(934));
		assertEquals("Nine Hundred Ninety Nine", NumbersInWords.numberToWords(999));
	}
}
